
# directories
SRCDIR = ../src
BUILDDIR = build
INCDIR = ../inc

# Compiler to use
CC = gcc

# Include paths for header files
INCLUDES = -I$(INCDIR)

# Compiler flags
CFLAGS = -Wall -Wextra -g $(INCLUDES) -std=gnu11

# extra gcc flags used to build dependency files
DEPFLAGS = -MMD -MP

# Paths to required libraries (-L flags)
LFLAGS =

# The specific libraries that project depends on (-l flags)
# don't inlclude lib part of library ie libtte should use -ltte
LIBS =

# All source files
SRCS = $(wildcard $(SRCDIR)/*.c)

# All object files
OBJS := $(SRCS:$(SRCDIR)/%.c=%.o)

# dependency files (used to rebuild when a header file changes)
DEPS := $(SRCS:$(SRCDIR)/%.c=%.d)

# name of executable
MAIN = {{cookiecutter.executable}}

# this is the default target
## compile main executable
all: $(MAIN)

$(MAIN): $(OBJS)
	@echo "Compiling executable: $(MAIN)"
	$(CC) $(CFLAGS) $(LFLAGS) $(LIBS) -o $(MAIN) $(OBJS)

# Automatically builds all object files from source files
# -c option compiles but does not link (create object files)
# -o is output filename
$(OBJS): %.o : $(SRCDIR)/%.c
	@echo "Compiling object file: $@"
	$(CC) $(CFLAGS) $(DEPFLAGS) -c $< -o $@

# unit test setup
TESTDIR = ../test
TESTFLAGS = -lcheck -I ../test
TESTSRCS = $(wildcard $(TESTDIR)/*.c) #add in any .c files from test dir
DEPSTEST := $(TESTSRCS:$(TESTDIR)/%.c=%.d)
TESTOBJS := $(TESTSRCS:$(TESTDIR)/%.c=%.o) #convert names of all .c files to .o
MAINOBJS := $(filter-out main.o, $(OBJS)) #remove main.o from list of .o files

$(TESTOBJS): %.o : $(TESTDIR)/%.c #compile but don't link .o files
	@echo "Compiling object file: $@"
	$(CC) $(CFLAGS) $(DEPFLAGS) -c $< -o $@

# unit test executable
TESTMAIN = test_{{cookiecutter.executable}}

# check target - compile the executable for running unit tests
$(TESTMAIN): $(TESTOBJS) $(MAINOBJS)
	@echo "Compiling unit test executable: $(TESTMAIN)"
	$(CC) $(CFLAGS) -o $(TESTMAIN) $(TESTOBJS) $(MAINOBJS) $(LFLAGS) $(LIBS) $(TESTFLAGS)

# build unit test executable
# makefile info: structure using $TESTMAIN here and above so target isn't rebuilt if no changes
# this is the same way the 'all' target is set up
## compile executable file for unit tests
test: $(TESTMAIN)

# include dependency files, must be after first target
-include $(DEPS) $(DEPSTEST)

# help statements should appear in the line directly above the target or on the same line
# help statements begin with two # symbols eg ##
# source for help target - https://gist.github.com/rcmachado/af3db315e31383502660
## print help dialog
help:
	$(info Available targets)
	@awk '/^[a-zA-Z\-\_0-9]+:/ {                                       \
          nb = sub( /^## /, "", helpMsg );                             \
          if(nb == 0) {                                                \
            helpMsg = $$0;                                             \
            nb = sub( /^[^:]*:.* ## /, "", helpMsg );                  \
          }                                                            \
          if (nb)                                                      \
            printf "\033[1;31m%-" width "s\033[0m %s\n", $$1, helpMsg; \
        }                                                              \
        { helpMsg = $$0 }'                                             \
        width=$$(grep -o '^[a-zA-Z_0-9]\+:' $(MAKEFILE_LIST) | wc -L)  \
        $(MAKEFILE_LIST)

## generate doxygen documentation in doc/ folder
doc:
	@echo "Generating documentation..."
	doxygen ../doc/doxygen/doxygen.cfg

## delete all temporary files and executables if they exist
clean:
	@echo "Deleting temporary files..."
	rm -rf -- *.o *.d *.dSYM $(MAIN) $(TESTMAIN)

.PHONY: help clean doc all test
