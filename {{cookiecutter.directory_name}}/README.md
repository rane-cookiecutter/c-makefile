@mainpage Homepage
@tableofcontents

# General Information

@todo add general project description here

## Requirements

- GNU Make (version 4.2.1)

### Optional requirements

- Doxygen (version 1.8.14)
    - [Documentation](http://www.doxygen.nl/manual/index.html)
- [C check framework](https://github.com/libcheck/check) (version 0.12.0)
    - [Installation](https://libcheck.github.io/check/web/install.html)
    - [Documentation](https://libcheck.github.io/check/doc/check_html/index.html#Top)

## Building

To compile the program executable run `make` from the `build/` directory.
Run `make help` to see the other availabe targets.

## Unit Tests

All unit tests should reside in the `test/` directory.
To build the unit test executable run `make test` from within the `build/` directory.
The compiled executable will run all available tests.

To run individual test suites or test cases export the environment variables `CK_RUN_SUITE`
and `CK_RUN_CASE` using a string set to the test suite or test case name.

## Doxygen documentation

Running `make doc` will generate html documentation in `doc/doxygen/html`
Open the `index.html` file to access the homepage.
This README doubles as the homepage.
