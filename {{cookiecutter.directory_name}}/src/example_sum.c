/**
 * @file example_sum.c
 * @author {{cookiecutter.author}}
 *
 * @todo Delete this file, it is here as an example for unit tests
 **/

#include "example_sum.h"

int sum(int x, int y)
{
    return x + y;
}
