/**
 * @file main.c
 * @author {{cookiecutter.author}}
 */

#include <stdio.h>
#include <stdlib.h>

/**
 * @brief program entry point
 * @param[in] argc the number of command line arguments
 * @param[in] argv strings of the actual command line arguments
 * @returns 0 for success, else non-zero value
 **/
int main(int argc, char *argv[])
{
    /// @todo Your code here
    printf("Hello world!\n");

    return 0;
}
