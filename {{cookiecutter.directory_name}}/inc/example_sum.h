/**
 * @file example_sum.h
 * @author {{cookiecutter.author}}
 *
 * @todo Delete this file, it is here as an example for unit tests
 **/

#ifndef EXAMPLE_SUM_H
#define EXAMPLE_SUM_H

/**
 * @brief add two integers and return the result
 *
 * @param[in] x first integer
 * @param[in] y second integer
 * @returns the sum x+y
 **/
int sum(int x, int y);

#endif // EXAMPLE_SUM_H
