/**
 * @file main_test.c
 * @author {{cookiecutter.author}}
 */

#include "main_test.h"

/**
 * @brief unit test master suite
 *
 * All sub-suites which contain individual tests are added to the master suite.
 * The default behavior is to run all test suites and all unit tests.
 * Individual suites or individual tests can be run by setting an environment
 * variable equal to the name of the test suite (CK_RUN_SUITE) or the individual
 * test name (CK_RUN_CASE). E.g. export CK_RUN_SUITE=example_suite_1
 * Use unset CK_RUN_SUITE to re-enable all tests.
 * See the below link for the full documentation:
 * https://libcheck.github.io/check/doc/check_html/check_toc.html#SEC_Contents
 *
 * @returns EXIT_SUCCESS or EXIT_FAILURE
 */
int main(void)
{
    // variables
    int number_failed;
    SRunner *sr;

    // create the master suite
    Suite *master = suite_create("{{cookiecutter.project_name}} Master Suite:");
    sr = srunner_create(master);

    /// @todo add any sub-suites here, delete these 2 examples
    srunner_add_suite(sr, example_suite_1());
    srunner_add_suite(sr, example_suite_2());

    // run tests
    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);

    // free all allocated resources
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
