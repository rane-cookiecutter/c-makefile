/**
 * @file main_test.h
 * @author {{cookiecutter.author}}
 *
 **/

#ifndef UNIT_TESTS_H
#define UNIT_TESTS_H

#include <check.h>
#include <stdio.h>
#include <stdlib.h>

/// @todo add any local includes needed for the tests

/// @todo add any sub-suites that will be added to the master

/// @todo delete this include, it is here as example
#include "example_sum.h"

/// @todo delete these sub-suites, they are here as an example
Suite *example_suite_1(void);
Suite *example_suite_2(void);

#endif // UNIT_TESTS_H
