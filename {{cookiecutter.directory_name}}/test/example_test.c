/**
 * @file example_test.c
 * @author {{cookiecutter.author}}
 *
 * @todo Delete this file, it is here as an example
 **/

#include "main_test.h"

/**
 * @brief unit test to check the sum function
 * @returns pass or fail
 */
START_TEST (test_sum1)
{
    int res = 0;

    // call the function being tested
    res = sum(2, 2);

    // verify the result is as expected
    ck_assert_int_eq(res, 4);
}
END_TEST

/**
 * @brief unit test to check the sum function
 *
 * Multiple checks can be done in the same test.
 *
 * @returns pass or fail
 */
START_TEST (test_sum2)
{
    int res = 0;

    // call the function being tested
    res = sum(1, -2);

    // verify the result is as expected
    ck_assert_int_eq(res, -1);

    // multiple checks in the same test
    res = sum(10,20);
    ck_assert_int_eq(res, 30);
}
END_TEST

/**
 * @brief test suite for the sum function
 *
 * This example shows how to add multiple tests to a single suite.
 *
 * @returns a test suite
 */
Suite * example_suite_1(void)
{
    // create the test suite
    Suite *s = suite_create("example_suite_1");

    // create a test case for each unit test
    TCase *tc_sum1 = tcase_create("Verify the fist sum");
    TCase *tc_sum2 = tcase_create("Verify the second sum");

    // add each unit test to its test case (the name used with START_TEST(<name>))
    tcase_add_test(tc_sum1, test_sum1);
    tcase_add_test(tc_sum2, test_sum2);

    // add each test case to the suite
    suite_add_tcase(s, tc_sum1);
    suite_add_tcase(s, tc_sum2);

    return s;
}

/**
 * @brief test suite for the sum function
 *
 * The same test "test_sum2" is run here as was used in the first suite.
 * Normally a new suite would test a different function or completely different functionality.
 *
 *  * @returns a test suite
 */
Suite * example_suite_2(void)
{
    // create the test suite
    Suite *s = suite_create("example_suite_2");

    // create a test case for each unit test
    TCase *tc_sum1 = tcase_create("Verify the fist sum");

    // add each unit test to its test case (the name used with START_TEST(<name>))
    tcase_add_test(tc_sum1, test_sum1);

    // add each test case to the suite
    suite_add_tcase(s, tc_sum1);

    return s;
}
