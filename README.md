# Description

[Cookiecutter](https://github.com/audreyr/cookiecutter) template to generate
a basic C project that compiles with a single Makefile.

To use the template run `cookiecutter https://gitlab.com/rane-cookiecutter/c-makefile.git`

Make sure to review the source files for Doxygen `@todo` comments. Run something like
`grep -rn '@todo` from the top level directory to print out the locations of all todo
statements.

## Requirements

- GNU Make (version 4.2.1)

### Optional requirements

- Doxygen (version 1.8.14)
    - [Documentation](http://www.doxygen.nl/manual/index.html)
- [C check framework](https://github.com/libcheck/check) (version 0.12.0)
    - [Installation](https://libcheck.github.io/check/web/install.html)
    - [Documentation](https://libcheck.github.io/check/doc/check_html/index.html#Top)

## Example unit tests

The example unit test files listed below demonstrate how the c check framework works.
You should rename and modify these files to suite your project needs.

- `src/example_sum.c` -- the function to test
- `inc/example_sum.h` -- the function declaration
- `test/example_test.c` -- the unit tests for the function

When creating new tests and test suites create new files in the `test/` directory that
test related functionality. This is preferable to using a single file for all unit
tests.

**NOTE:** the Makefile assumes the filename `main.c` contains `main()`. If the main
entry point is placed in a different file make sure to modify the Makefile to filter out
the new filename for unit tests. Otherwise there will be two `main()`s and things won't
work. Look for this line:

```make
MAINOBJS := $(filter-out main.o, $(OBJS))
```
